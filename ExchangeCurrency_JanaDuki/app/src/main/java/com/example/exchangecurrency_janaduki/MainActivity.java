package com.example.exchangecurrency_janaduki;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

    private ListView mListView;
    private TextView textview;
    Call<List<EValue>> apiCall;

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private FragmentManager mFragmentManager;

    private  CalculateFragment mCalculateFragment;
    private  HistoryOfConversion mHistoryOfConversion;
    private ExchangeRate mExchangeRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        initViews();
        setUpPager();
        //za fragment_exchange
        //setUpApiCallE();
    }

    private void setUpPager() {
        List<Fragment> fragmentList = new ArrayList();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        mExchangeRate = ExchangeRate.newInstance();
        mCalculateFragment = CalculateFragment.newInstance();
        mHistoryOfConversion = HistoryOfConversion.newInstance();
        fragmentList.add(mExchangeRate);
        fragmentList.add(mCalculateFragment);
        fragmentList.add(mHistoryOfConversion);
        fragmentTransaction.commit();
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(),fragmentList);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(3);
        Log.w("MAINTHREAD", "Usao sam evo upravo sada!");
        mExchangeRate.setFragmentsInExchangeRate(mCalculateFragment, mHistoryOfConversion);

    }

    private void initViews() {
        mViewPager = findViewById(R.id.viewPager);
        mTabLayout = findViewById(R.id.tabLayoout);
    }

//    private void setUpApiCallE() {
//        Call<EValue> apiCall = NetworkUtils.getApiInterface().getEValues(); //nertwork utils je pomocna klasa koja vracsa instancu interface koje ima jednu metdu trenutno
//        Log.w("API", "JEsam tu sad !");
//        apiCall.enqueue(new Callback<EValue>() {
//            @Override //okida se kada je doslo do komunikacije sa serverom
//            public void onResponse(Call<EValue> call, Response<EValue> response) {
//                if(response.isSuccessful() && response.body() != null){
//                    //textview.setText(response.body().toString());
//                    showSpecificValues(response.body()); //mora bit main activity
//                }
//            }
//            @Override //greska u postavljanju interneta, prije nego je doslo do komunikacije sa serverom
//            public void onFailure(Call<EValue> call, Throwable t) {
//                Toast.makeText(MainActivity.this, t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private void showSpecificValues(EValue body) {
//        Rates rates = body.getRates();
//        mCalculateFragment.setRates(rates);
//        mCalculateFragment.setmHistoryOfConversion(mHistoryOfConversion);
//        Double USD = 1/rates.getUSD();
//        Double GBP = 1/rates.getGBP();
//        Double EUR = 1/rates.getEUR();
//        Double CZK = 1/rates.getCZK();
//        Double ISK = 1/rates.getISK();
//
//        TextView tvUSD = findViewById(R.id.tvJsonDataUSD);
//        TextView tvGBP = findViewById(R.id.tvJsonDataGBP);
//        TextView tvEUR = findViewById(R.id.tvJsonDataEUR);
//        TextView tvCZK = findViewById(R.id.tvJsonDataCZK);
//        TextView tvISK = findViewById(R.id.tvJsonDataISK);
//
//        tvUSD.setText("USD to kn is: \n " + USD);
//        tvGBP.setText("GBP to kn is: \n " + GBP);
//        tvEUR.setText("EUR to kn is: \n " + EUR);
//        tvCZK.setText("CZK to kn is: \n " + CZK);
//        tvISK.setText("ISK to kn is: \n " + ISK);
//
//        ImageView imUSD = findViewById(R.id.imUSD);
//        ImageView imGBP = findViewById(R.id.imGBP);
//        ImageView imEUR = findViewById(R.id.imEUR);
//        ImageView imCZK = findViewById(R.id.imCZK);
//        ImageView imISK = findViewById(R.id.imISK);
//
//        imUSD.setImageResource(R.drawable.usd);
//        imGBP.setImageResource(R.drawable.gbp);
//        imEUR.setImageResource(R.drawable.euro);
//        imCZK.setImageResource(R.drawable.czk);
//        imISK.setImageResource(R.drawable.isk);
//    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(apiCall != null){
            apiCall.cancel();
        }
    }


}