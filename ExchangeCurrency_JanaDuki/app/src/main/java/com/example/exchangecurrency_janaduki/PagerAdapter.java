package com.example.exchangecurrency_janaduki;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class PagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mItems;

    public PagerAdapter(@NonNull FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        mItems = fragmentList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    public CharSequence getPageTitle(int position){
        if(position == 0){
            return "Exchange rate";
        }else if(position == 1){
            return "Calculate value";
        }else{
            return "History of conversion";
        }
    }

}
