package com.example.exchangecurrency_janaduki;

public interface ImageClickListener {
    void onImageClick(int position);
}
