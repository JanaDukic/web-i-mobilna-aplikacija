package com.example.exchangecurrency_janaduki;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {


    //@GET("hrk/{code}")
    //Call<Value> getValue(@Path("code") String valueCode);

    @GET("latest?base=HRK")  //latest/?symbols=USD ->radi
    Call<EValue> getEValues();

    @GET("latest/?symbols={rates}")
    Call<EValue> getEValuesSpecific(@Query("rates") String rateValue);

}
