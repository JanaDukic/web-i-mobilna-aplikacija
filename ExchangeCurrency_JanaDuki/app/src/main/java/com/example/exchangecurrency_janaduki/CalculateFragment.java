package com.example.exchangecurrency_janaduki;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class CalculateFragment extends Fragment {

    private Spinner spinnerFROM;
    private Spinner spinnerTO;
    private EditText mResult;
    private EditText mValue;
    private Button mbtnConvert;
    private Rates mRates;
    private ButtonClickListener buttonClickListener;
    private HistoryOfConversion mHistoryOfConversion;

    public void setRates(Rates ratess){ this.mRates = ratess;}
    public void setmHistoryOfConversion( HistoryOfConversion historyFragment) {
        this.mHistoryOfConversion = historyFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calculate, container, false);
    }

    public  static CalculateFragment newInstance(){
        CalculateFragment fragment = new CalculateFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinnerFROM = view.findViewById(R.id.spinner1);
        spinnerTO = view.findViewById(R.id.spinner2);
        mResult = view.findViewById(R.id.etDecimalResult);
        mValue = view.findViewById(R.id.etDecimalValue);
        mbtnConvert = view.findViewById(R.id.btnCalculateValue);
        String[] arraySpinner = new String[]{ "HRK", "EURO", "USD", "GBP", "CZK", "ISK"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity().getBaseContext(), android.R.layout.simple_spinner_dropdown_item, arraySpinner);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFROM.setAdapter(arrayAdapter);
        spinnerTO.setAdapter(arrayAdapter);
        mResult.setFocusable(false); // za undo je mResult.setFocusableInTouchMode(true);
        //mResult.setText(spinnerFROM.getSelectedItem().toString());
        setUpListeners();
    }

    private void setUpListeners() {
        mbtnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mValue.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(),"Please insert decimal value for button CONVERT to work", Toast.LENGTH_LONG).show();
                }else {
                    doConversion();

                }
            }
        });
    }

    private void doConversion() {
        String valueFROM = spinnerFROM.getSelectedItem().toString();
        String valueTO = spinnerTO.getSelectedItem().toString();
        String Snumber = mValue.getText().toString();
        Double number = Double.parseDouble(Snumber);
        Double broj = 0.0;

        if(valueFROM.equals("HRK")){
            switch (valueTO){
                case "HRK": broj = number*(mRates.getHRK());
                            mResult.setText(broj.toString()); break;
                case "EURO": broj = number*(mRates.getEUR());
                             mResult.setText(broj.toString()); break;
                case "USD": broj = number*(mRates.getUSD());
                            mResult.setText(broj.toString()); break;
                case "GBP": broj = number*(mRates.getGBP());
                            mResult.setText(broj.toString()); break;
                case "CZK": broj = number*(mRates.getCZK());
                            mResult.setText(broj.toString()); break;
                case "ISK": broj = number*(mRates.getISK());
                            mResult.setText(broj.toString()); break;
            }
        }else if(valueFROM.equals("EURO")){
            switch (valueTO){
                case "HRK": broj = number*(1/mRates.getEUR());
                            mResult.setText(broj.toString()); break;
                case "EURO": broj = number;
                            mResult.setText(broj.toString()); break;
                case "USD": broj = number*(1/mRates.getEUR());
                            broj = broj*(mRates.getUSD());
                            mResult.setText(broj.toString()); break;
                case "GBP": broj = number*(1/mRates.getEUR());
                            broj = broj*(mRates.getGBP());
                            mResult.setText(broj.toString()); break;
                case "CZK": broj = number*(1/mRates.getEUR());
                            broj = broj*(mRates.getCZK());
                            mResult.setText(broj.toString()); break;
                case "ISK": broj = number*(1/mRates.getEUR());
                            broj = broj*(mRates.getISK());
                            mResult.setText(broj.toString()); break;
            }
        }else if(valueFROM.equals("USD")){
            switch (valueTO){
                case "HRK": broj = number*(1/mRates.getUSD());
                            mResult.setText(broj.toString()); break;
                case "EURO": broj = number*(1/mRates.getUSD());
                             broj = broj*(mRates.getEUR());
                            mResult.setText(broj.toString()); break;
                case "USD": broj = number;
                            mResult.setText(broj.toString()); break;
                case "GBP": broj = number*(1/mRates.getUSD());
                            broj = broj*(mRates.getGBP());
                            mResult.setText(broj.toString()); break;
                case "CZK": broj = number*(1/mRates.getUSD());
                            broj = broj*(mRates.getCZK());
                            mResult.setText(broj.toString()); break;
                case "ISK": broj = number*(1/mRates.getUSD());
                            broj = broj*(mRates.getISK());
                            mResult.setText(broj.toString()); break;
            }
        }else if(valueFROM.equals("GBP")){
            switch (valueTO){
                case "HRK": broj = number*(1/mRates.getGBP());
                             mResult.setText(broj.toString()); break;
                case "EURO": broj = number*(1/mRates.getGBP());
                            broj = broj*(mRates.getEUR());
                             mResult.setText(broj.toString()); break;
                case "USD": broj = number*(1/mRates.getGBP());
                            broj = broj*(mRates.getUSD());
                            mResult.setText(broj.toString()); break;
                case "GBP": broj = number;
                            mResult.setText(broj.toString()); break;
                case "CZK": broj = number*(1/mRates.getGBP());
                            broj = broj*(mRates.getCZK());
                            mResult.setText(broj.toString()); break;
                case "ISK": broj = number*(1/mRates.getGBP());
                            broj = broj*(mRates.getISK());
                            mResult.setText(broj.toString()); break;
            }
        }else if(valueFROM.equals("CZK")){
            switch (valueTO){
                case "HRK": broj = number*(1/mRates.getCZK());
                            mResult.setText(broj.toString()); break;
                case "EURO": broj = number*(1/mRates.getCZK());
                            broj = broj*(mRates.getEUR());
                            mResult.setText(broj.toString()); break;
                case "USD": broj = number*(1/mRates.getCZK());
                            broj = broj*(mRates.getUSD());
                            mResult.setText(broj.toString()); break;
                case "GBP": broj = number*(1/mRates.getCZK());
                            broj = broj*(mRates.getGBP());
                            mResult.setText(broj.toString()); break;
                case "CZK": broj = number;
                            mResult.setText(broj.toString()); break;
                case "ISK": broj = number*(1/mRates.getCZK());
                            broj = broj*(mRates.getISK());
                            mResult.setText(broj.toString()); break;
            }
        }else if(valueFROM.equals("ISK")){
            switch (valueTO){
                case "HRK": broj = number*(1/mRates.getISK());
                            mResult.setText(broj.toString()); break;
                case "EURO": broj = number*(1/mRates.getISK());
                            broj = broj*(mRates.getEUR());
                            mResult.setText(broj.toString()); break;
                case "USD": broj = number*(1/mRates.getISK());
                            broj = broj*(mRates.getUSD());
                            mResult.setText(broj.toString()); break;
                case "GBP": broj = number*(1/mRates.getISK());
                            broj = broj*(mRates.getGBP());
                            mResult.setText(broj.toString()); break;
                case "CZK": broj = number*(1/mRates.getISK());
                            broj = broj*(mRates.getCZK());
                            mResult.setText(broj.toString()); break;
                case "ISK": broj = number;
                            mResult.setText(broj.toString()); break;
            }
        }
        mHistoryOfConversion.addToHistory(mValue, mResult, valueFROM, valueTO);

    }


}