package com.example.exchangecurrency_janaduki;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExchangeRate extends Fragment {

    private View view;
    private  CalculateFragment mCalculateFragment;
    private  HistoryOfConversion mHistoryOfConversion;

    public void setFragmentsInExchangeRate(CalculateFragment calculateFragment, HistoryOfConversion historyOfConversion){
        this.mCalculateFragment = calculateFragment;
        this.mHistoryOfConversion = historyOfConversion;
        setUpApiCallE();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Log.w("EXCHANGERATE", "Usao sam evo upravo sada!")
        view =  inflater.inflate(R.layout.fragment_exchange_rate, container, false);
        return view;
    }

    public static ExchangeRate newInstance(){
        ExchangeRate fragment = new ExchangeRate();
        return fragment;
    }

    private void setUpApiCallE() {
        Call<EValue> apiCall = NetworkUtils.getApiInterface().getEValues(); //nertwork utils je pomocna klasa koja vracsa instancu interface koje ima jednu metdu trenutno
        Log.w("API", "JEsam tu sad !");
        apiCall.enqueue(new Callback<EValue>() {
            @Override //okida se kada je doslo do komunikacije sa serverom
            public void onResponse(Call<EValue> call, Response<EValue> response) {
                if(response.isSuccessful() && response.body() != null){
                    //textview.setText(response.body().toString());
                    showSpecificValues(response.body()); //mora bit main activity
                }
            }
            @Override //greska u postavljanju interneta, prije nego je doslo do komunikacije sa serverom
            public void onFailure(Call<EValue> call, Throwable t) {
                Toast.makeText(getActivity().getBaseContext(), t.getLocalizedMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showSpecificValues(EValue body) {
        Rates rates = body.getRates();
        mCalculateFragment.setRates(rates);
        mCalculateFragment.setmHistoryOfConversion(mHistoryOfConversion);
        Double USD = 1/rates.getUSD();
        Double GBP = 1/rates.getGBP();
        Double EUR = 1/rates.getEUR();
        Double CZK = 1/rates.getCZK();
        Double ISK = 1/rates.getISK();

        TextView tvUSD = view.findViewById(R.id.tvJsonDataUSD);
        TextView tvGBP = view.findViewById(R.id.tvJsonDataGBP);
        TextView tvEUR = view.findViewById(R.id.tvJsonDataEUR);
        TextView tvCZK = view.findViewById(R.id.tvJsonDataCZK);
        TextView tvISK = view.findViewById(R.id.tvJsonDataISK);

        tvUSD.setText("USD to kn is: \n " + USD);
        tvGBP.setText("GBP to kn is: \n " + GBP);
        tvEUR.setText("EUR to kn is: \n " + EUR);
        tvCZK.setText("CZK to kn is: \n " + CZK);
        tvISK.setText("ISK to kn is: \n " + ISK);

        ImageView imUSD = view.findViewById(R.id.imUSD);
        ImageView imGBP = view.findViewById(R.id.imGBP);
        ImageView imEUR = view.findViewById(R.id.imEUR);
        ImageView imCZK = view.findViewById(R.id.imCZK);
        ImageView imISK = view.findViewById(R.id.imISK);

        imUSD.setImageResource(R.drawable.usd);
        imGBP.setImageResource(R.drawable.gbp);
        imEUR.setImageResource(R.drawable.euro);
        imCZK.setImageResource(R.drawable.czk);
        imISK.setImageResource(R.drawable.isk);
    }


}