package com.example.exchangecurrency_janaduki;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rates {

    @SerializedName("CAD")
    @Expose
    private Double cAD;
    @SerializedName("HKD")
    @Expose
    private Double hKD;
    @SerializedName("ISK")
    @Expose
    private Double iSK;
    @SerializedName("PHP")
    @Expose
    private Double pHP;
    @SerializedName("DKK")
    @Expose
    private Double dKK;
    @SerializedName("HUF")
    @Expose
    private Double hUF;
    @SerializedName("CZK")
    @Expose
    private Double cZK;
    @SerializedName("GBP")
    @Expose
    private Double gBP;
    @SerializedName("RON")
    @Expose
    private Double rON;
    @SerializedName("SEK")
    @Expose
    private Double sEK;
    @SerializedName("IDR")
    @Expose
    private Double iDR;
    @SerializedName("INR")
    @Expose
    private Double iNR;
    @SerializedName("BRL")
    @Expose
    private Double bRL;
    @SerializedName("RUB")
    @Expose
    private Double rUB;
    @SerializedName("HRK")
    @Expose
    private Double hRK;
    @SerializedName("JPY")
    @Expose
    private Double jPY;
    @SerializedName("THB")
    @Expose
    private Double tHB;
    @SerializedName("CHF")
    @Expose
    private Double cHF;
    @SerializedName("EUR")
    @Expose
    private Double eUR;
    @SerializedName("MYR")
    @Expose
    private Double mYR;
    @SerializedName("BGN")
    @Expose
    private Double bGN;
    @SerializedName("TRY")
    @Expose
    private Double tRY;
    @SerializedName("CNY")
    @Expose
    private Double cNY;
    @SerializedName("NOK")
    @Expose
    private Double nOK;
    @SerializedName("NZD")
    @Expose
    private Double nZD;
    @SerializedName("ZAR")
    @Expose
    private Double zAR;
    @SerializedName("USD")
    @Expose
    private Double uSD;
    @SerializedName("MXN")
    @Expose
    private Double mXN;
    @SerializedName("SGD")
    @Expose
    private Double sGD;
    @SerializedName("AUD")
    @Expose
    private Double aUD;
    @SerializedName("ILS")
    @Expose
    private Double iLS;
    @SerializedName("KRW")
    @Expose
    private Double kRW;
    @SerializedName("PLN")
    @Expose
    private Double pLN;

    public Double getCAD() {
        return cAD;
    }

    public Double getHKD() {
        return hKD;
    }

     public Double getISK() {
        return iSK;
    }

    public Double getPHP() {
        return pHP;
    }

     public Double getDKK() {
        return dKK;
    }

    public Double getHUF() {
        return hUF;
    }

    public Double getCZK() {
        return cZK;
    }

    public Double getGBP() {
        return gBP;
    }

    public Double getRON() {
        return rON;
    }

    public Double getSEK() {
        return sEK;
    }

    public Double getIDR() {
        return iDR;
    }

    public Double getINR() {
        return iNR;
    }

    public Double getBRL() {
        return bRL;
    }

    public Double getRUB() {
        return rUB;
    }


    public Double getHRK() {
        return hRK;
    }

    public Double getJPY() {
        return jPY;
    }

    public Double getTHB() {
        return tHB;
    }

    public Double getCHF() {
        return cHF;
    }


    public Double getEUR() {
        return eUR;
    }

    public Double getMYR() {
        return mYR;
    }

    public Double getBGN() {
        return bGN;
    }


    public Double getTRY() {
        return tRY;
    }

    public Double getCNY() {
        return cNY;
    }


    public Double getNOK() {
        return nOK;
    }

    public Double getNZD() {
        return nZD;
    }

    public Double getZAR() {
        return zAR;
    }

    public Double getUSD() {
        return uSD;
    }

    public Double getMXN() {
        return mXN;
    }

    public Double getSGD() {
        return sGD;
    }

    public Double getAUD() {
        return aUD;
    }

    public Double getILS() {
        return iLS;
    }

    public Double getKRW() {
        return kRW;
    }

    public Double getPLN() {
        return pLN;
    }

}
