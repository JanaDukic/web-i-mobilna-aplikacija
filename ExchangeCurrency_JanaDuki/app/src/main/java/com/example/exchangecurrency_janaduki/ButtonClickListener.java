package com.example.exchangecurrency_janaduki;

public interface ButtonClickListener {
    void onButtonClicked(String message);
}
