package com.example.exchangecurrency_janaduki;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class HistoryOfConversion extends Fragment  implements ImageClickListener{

    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdaper;
    private EditText mEditText;
    private ImageButton mImageButton;

    private EditText mValue;
    private EditText mResult;
    private String mSpinnerFROM;
    private String mSpinnerTO;

    public static HistoryOfConversion newInstance(){
        HistoryOfConversion fragment = new HistoryOfConversion();
        return fragment;
    }

    public void addToHistory(EditText value, EditText result, String from, String to){
        this.mValue = value;
        this.mResult = result;
        this.mSpinnerFROM = from;
        this.mSpinnerTO = to;
        addCell();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_of_conversion, container, false);
        setUpRecycler(view);
        setUpRecyclerData();

        return view;
    }

    private void setUpRecycler(View view) {
        mRecyclerView = view.findViewById(R.id.recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdaper = new RecyclerAdapter(this);
        mRecyclerView.setAdapter(mAdaper);
        this.mImageButton = view.findViewById(R.id.tvX);

    }

    private void setUpRecyclerData() {
        List<String> data = new ArrayList<>();
        //data.add("Jana");
        mAdaper.addData(data);
    }

    public void addCell() {
        String line = mSpinnerFROM + ": " + mValue.getText().toString() + " is \n" + mSpinnerTO + ": " + mResult.getText().toString();
        mAdaper.addNewCell(line);

    }

    @Override
    public void onImageClick(int position) {
        mAdaper.removeCell(position);
    }
}