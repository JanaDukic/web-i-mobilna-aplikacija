package com.example.exchangecurrency_janaduki;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageButton mImageButton;
    private ImageClickListener mImageClickListener;
    private int position;
    private TextView mTextView;

    public NameViewHolder(@NonNull View itemView, ImageClickListener listener) {
        super(itemView);
        this.mImageClickListener =  listener;
        this.mImageButton = itemView.findViewById(R.id.tvX);
        this.mImageButton.setOnClickListener(this);
        this.mTextView = itemView.findViewById(R.id.tvName);
    }
    public void setTvName(String name){
        mTextView.setText(name);
    }

    @Override
    public void onClick(View v) {
        position = getAdapterPosition();
        mImageClickListener.onImageClick(position);
    }
}
